<?php

declare(strict_types = 1);

namespace Drupal\ckeditor5_chatgpt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Ckeditor5 chatgpt routes.
 */
final class Ckeditor5ChatgptController extends ControllerBase {

  /**
   * Builds the chatGPT proxy response.
   */
  public function proxy(Request $request): JsonResponse {
    $apiKey = $this->config('ckeditor5_chatgpt.settings')->get('ApiKey');

    $whatIWant = $this->config('ckeditor5_chatgpt.settings')->get('WhatIWant');

    $data = json_decode($request->getContent(), TRUE);

    if (is_null($data) || !key_exists("text", $data) || empty($data['text'])) {
      return new JsonResponse(["error" => $this->t('Provide a "text" value in your request.')], 201);
    }

    $numberOfWords = str_word_count($data['text']);

    $context = [
      'model' => 'gpt-3.5-turbo',
      'messages' => [
        [
          'role' => 'user',
          'content' => $whatIWant . "\n" . $data["text"],
        ],
      ],
      'max_tokens' => $numberOfWords + 100,
    ];

    $result = ['content' => NULL];

    $client = \OpenAI::client($apiKey);

    try {
      $response = $client->chat()->create($context);

      if (isset($response->choices[0]->message->content)) {
        $result = [
          'content' => $response->choices[0]->message->content,
        ];
      }
    }
    catch (\Exception $e) {
      $result = [
        'error' => $e->getMessage(),
      ];
    }

    return new JsonResponse($result);
  }

}
