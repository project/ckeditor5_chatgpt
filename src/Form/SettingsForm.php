<?php

declare(strict_types = 1);

namespace Drupal\ckeditor5_chatgpt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure ckeditor5-chatGPT settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  const BASE_API_KEY = 'Provide an API key';
  const BASE_WHAT_I_WANT = 'Correct the spelling of this text following its own language.';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'ckeditor5_chatgpt_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['ckeditor5_chatgpt.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['ApiKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $this->config('ckeditor5_chatgpt.settings')->get('ApiKey') ?? self::BASE_API_KEY,
    ];

    $form['WhatIWant'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Describe what you want'),
      '#default_value' => $this->config('ckeditor5_chatgpt.settings')->get('WhatIWant') ?? self::BASE_WHAT_I_WANT,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('ApiKey') === self::BASE_API_KEY || empty($form_state->getValue('ApiKey'))) {
      $form_state->setErrorByName(
        'message',
        $this->t('The value of the API Key is not correct.'),
      );
    }

    if (empty($form_state->getValue('WhatIWant'))) {
      $form_state->setErrorByName(
        'message',
        $this->t('The value of the description about what you want is not correct.'),
      );
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('ckeditor5_chatgpt.settings')
      ->set('ApiKey', $form_state->getValue('ApiKey'))
      ->set('WhatIWant', $form_state->getValue('WhatIWant'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
