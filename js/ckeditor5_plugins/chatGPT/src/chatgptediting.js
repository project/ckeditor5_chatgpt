import { Plugin } from 'ckeditor5/src/core'
import { Widget } from 'ckeditor5/src/widget'
import InsertChatGPTCommand from './insertchatgptcommand'

export default class ChatGPTEditing extends Plugin {
  static get requires () {
    return [Widget]
  }

  init () {
    this.editor.commands.add(
      'insertchatGPT',
      new InsertChatGPTCommand(this.editor)
    )
  }
}
