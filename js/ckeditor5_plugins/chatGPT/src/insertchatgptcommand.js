/**
 * @file defines InsertChatGPTCommand, which is executed when the chatGPT
 * toolbar button is pressed.
 */

import { Command } from 'ckeditor5/src/core'

export default class InsertChatGPTCommand extends Command {
  execute () {
    const editor = this.editor
    const data = editor.getData()

    fetch('/ckeditor5-chatgpt/proxy', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ text: data })
    })
      .then(response => response.json())
      .then(data => {
        editor.setData(data.content)
      })
      .catch((error) => {
        console.error('Error:', error)
      })
  }
}
